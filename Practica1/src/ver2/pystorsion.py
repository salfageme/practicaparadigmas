# -*- coding: UTF-8 -*-
'''
PRACTICA 2 - PARADIGMAS DE PROGRAMACION: DISTORSIÓN DE IMÁGENES
AUTORES:
    DANIEL BARBA GUTIÉRREZ - 1ºGºIng.Inf - L1
    SAMUEL ALFAGEME SAINZ - 1ºGºIng.Inf - L1

'''
import pygtk, gtk, os, DistorsionP2

class ImgDistorsion:

	def __init__(self):
		self.builder = gtk.Builder()
		self.builder.add_from_file("interfaz-pystorsion.glade")
		# Elementos de la interfaz necesarios en el codigo
		self.aux_rango_radio = self.builder.get_object("aux_rango_radio")
		self.aux_rango_factor = self.builder.get_object("aux_rango_factor")
		self.aux_lista = self.builder.get_object("aux_lista")
		self.lista = self.builder.get_object("lista")
		self.imagen = self.builder.get_object("imagen")
		self.about = self.builder.get_object("aboutdialog1")
		self.evt_img = self.builder.get_object("evt_img")
		self.st1 = self.builder.get_object("statusbar1")
		self.st2 = self.builder.get_object("statusbar2")
		self.desBtn = self.builder.get_object("toolbutton8")
		self.rehaBtn = self.builder.get_object("toolbutton9")
		self.saveBtn = self.builder.get_object("toolbutton2")
		self.saveBtnMenu = self.builder.get_object("guardar")
		self.normalBtnMenu = self.builder.get_object("reescalar")
		self.desBtnMenu = self.builder.get_object("deshacer")
		self.rehaBtnMenu = self.builder.get_object("rehacer")
		self.ampliarBtn = self.builder.get_object("ampliar_btn")
		self.reducirBtn = self.builder.get_object("reducir_btn")
		self.normalBtn = self.builder.get_object("normal_btn")		
		
		# Elementos auxiliares e inicializacion de valores
		self.listaBtns = [self.desBtn, self.rehaBtn, self.desBtnMenu, self.rehaBtnMenu,
						self.normalBtnMenu, self.ampliarBtn, self.reducirBtn, self.normalBtn,
						self.saveBtn, self.saveBtnMenu]
		self.evt_img.set_sensitive(False)
		self.img = None
		self.cordX = None
		self.cordY = None
		# Listas que contendrán pixelBuffers con estados pasados/presentes de la imagen.
		self.matrizDePixBuf = []
		self.matrizRehacer = []
		# Valores por defecto de los selectores de parametros.
		self.aux_rango_radio.set_value(50)
		self.aux_rango_factor.set_value(0.5)
		# Desactivar los botones del menu que no sean necesarios
		for boton in self.listaBtns:
			boton.set_sensitive(False)
		# Conectar las señales con sus manejadores
		self.builder.connect_signals(self)

	def abrir_fichero(self, widget, data=None):
		# Dialogo de apertura de ficheros, crea un objeto Image y lo muestra en la interfaz.
		dlg = gtk.FileChooserDialog(
			"Abrir fichero", None,
			gtk.FILE_CHOOSER_ACTION_OPEN,
			(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
			gtk.STOCK_OPEN, gtk.RESPONSE_OK))
		# Filtra los ficheros para mostrar unicamente imagenes en formatos soportados por GTK.
		filt = gtk.FileFilter()
		filt.set_name('Imagenes')
		filt.add_mime_type('image/png')
		filt.add_mime_type('image/jpeg')
		filt.add_pattern("*.png")
		filt.add_pattern("*.jpeg")
		dlg.add_filter(filt)
		if dlg.run() == gtk.RESPONSE_OK:
			nomfich = dlg.get_filename()
			self.img = DistorsionP2.Image(nomfich)
			# Matriz de distorsion inicial.
			self.img.setPreCoordMat(int(self.aux_rango_radio.get_value()),self.aux_rango_factor.get_value())
			# Elimina los datos de ejecuciones anteriores (si los hubiera).
			self.aux_lista.clear()
			del self.matrizDePixBuf[:]
			del self.matrizRehacer[:]
			# Activa los botones de reescalado/guardado y desactiva los de deshacer/rehacer.
			for boton in self.listaBtns[0:4]:
				boton.set_sensitive(False)
			for boton in self.listaBtns[4:]:
				boton.set_sensitive(True)
			self.imagen.set_from_pixbuf(self.img.pixBuf1)
			self.evt_img.set_sensitive(True)
			self.imagen.show()
			self.matrizDePixBuf.append((self.img.pixBuf2, False))
			self.st1.push(1,"Abierto "+nomfich)
		dlg.destroy()

	def guardar_fichero(self,widget,data=None):
		# Genera una ventana emergente para guardar la imagen modificada.
		dlg = gtk.FileChooserDialog(
			"Guardar Imagen", None,
			gtk.FILE_CHOOSER_ACTION_SAVE,
			(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
			gtk.STOCK_SAVE, gtk.RESPONSE_OK))
		if dlg.run() == gtk.RESPONSE_OK:
			nomfich = dlg.get_filename()
			# dlg.set_do_overwrite_confirmation(True)
			self.img.saveImage(nomfich)
			self.st1.push(5,"Imagen guardada en "+nomfich)
		dlg.destroy()

	def showAbout(self, event, data=None):
		# Muestra el diálogo de información sobre la aplicación.
		self.about.show()

	def closeDialog(self, window, event):
		# Oculta el diálogo "Acerca de PYSTORSION"
		self.about.hide()
		return True

	def actualizar_params(self, widget, data=None):
		# Si existe un objeto imagen, modifica los parámetros de su matriz de precoordenadas.
		if self.img:
			radio = int(self.aux_rango_radio.get_value())
			factor = self.aux_rango_factor.get_value()
			self.img.setPreCoordMat(radio, factor)

	def raton_pulsado(self, widget, event, data=None):
		# Aplica la distorsión con radio en la posición de pulsacion del ratón sobre la imagen.
		wgwidth, wgheight = widget.window.get_size()
		imwidth, imheight = self.img.getDimensions()
		self.cordX = int(event.x - (wgwidth -imwidth)/2)
		self.cordY = int(event.y - (wgheight -imheight)/2)
		self.ref_fil = self.aux_lista.append((self.cordX, self.cordY))
		transformacion = (self.cordX,self.cordY,int(self.aux_rango_radio.get_value()))
		self.img.applyTransform(transformacion)
		self.img.updatePixBuf()
		self.imagen.set_from_pixbuf(self.img.pixBuf2)
		del self.matrizRehacer[:]
		self.rehaBtn.set_sensitive(False)
		self.rehaBtnMenu.set_sensitive(False)
		self.st1.push(2,"Aplicando distorsion")

	def raton_arrastrado(self, widget, event, data=None):
		# Muestra la previsualización de la distorsión mientras se arrastra el ratón por la imagen.
		wgwidth, wgheight = widget.window.get_size()
		imwidth, imheight = self.img.getDimensions()
		self.cordX = int(event.x - (wgwidth -imwidth)/2)
		self.cordY = int(event.y - (wgheight -imheight)/2)
		self.img.pixArray2 = self.img.pixArray1.copy()
		transformacion = (self.cordX,self.cordY,int(self.aux_rango_radio.get_value()),self.aux_rango_factor.get_value())
		self.aux_lista.set_value(self.ref_fil,0,self.cordX)
		self.aux_lista.set_value(self.ref_fil,1,self.cordY)
		self.img.applyTransform(transformacion)
		self.img.updatePixBuf()
		self.imagen.set_from_pixbuf(self.img.pixBuf2)
		event.window.process_updates(True)
		
	def raton_soltado(self, widget, event, data=None):
		# Aplica la distorsión de manera definitiva, permitiendo deshacerla.
		self.desBtn.set_sensitive(True)
		self.desBtnMenu.set_sensitive(True)
		self.img.updatePixArray()
		self.matrizDePixBuf.append((self.img.pixBuf2,True))

	def ampliar(self, widget, data=None):
		# Aplica un reescalado (1:1.1)
		self.setScale(1.1)
		self.st1.push(5,"Ampliar")

	def reducir(self, widget, data=None):
		# Aplica un reescalado (1:0.9)
		self.setScale(0.9)
		self.st1.push(6,"Reducir")
		
	def normalSize(self, widget, data=None):
		# Devuelve a la imagen a su escala original
		self.setScale(1)
		self.st1.push(7,"Tamaño Original")
		
	def setScale(self, porcentaje):
		# Reescala y muestra la imagen actual en función del param. porcentaje.
		self.img.copyBasePixArray()
		self.img.reescalate(porcentaje)
		self.img.updatePixBuf()
		self.imagen.set_from_pixbuf(self.img.pixBuf2)
		self.desBtn.set_sensitive(True)
		self.desBtnMenu.set_sensitive(True)
		self.img.updatePixArray()
		self.matrizDePixBuf.append((self.img.pixBuf2,False))
		del self.matrizRehacer[:]
		self.rehaBtn.set_sensitive(False)
		self.rehaBtnMenu.set_sensitive(False)

	def deshacer(self, widget, data=None):
		# Retorna la imagen a un estado anterior definido en la matrizDePixBuf
		if len(self.matrizDePixBuf)>1 :
			# Comprabar si se trata de transformacion (coordenadas asociadas) o de reescalado
			if self.matrizDePixBuf[-1][1]:
				coor = (self.aux_lista[-1][0],self.aux_lista[-1][1])
				self.aux_lista.remove(self.aux_lista[-1].iter)
				self.matrizRehacer.append((coor,self.matrizDePixBuf[-1]))
			else:
				self.matrizRehacer.append((False,self.matrizDePixBuf[-1]))
			# Deshacer la transformacion/reescalado
			self.img.imgFromPixBuf(self.matrizDePixBuf[-2][0])
			self.matrizDePixBuf.remove(self.matrizDePixBuf[-1])
			self.img.updatePixBuf()
			self.imagen.set_from_pixbuf(self.img.pixBuf2)
			self.st1.push(3,"Deshacer")
			self.rehaBtn.set_sensitive(True)
			self.rehaBtnMenu.set_sensitive(True)
			if len(self.matrizDePixBuf)==1 :
				self.desBtn.set_sensitive(False)
				self.desBtnMenu.set_sensitive(False)

	def rehacer(self, widget, data=None):
		# Devuelve a la imagen a un estado futuro, en caso de haber vuelto a uno anterior.
		# Estructura pila LIFO: [coordenadas/False,[PixBuf,Transformacion/Reescalado]]
		# Incluir las coordenadas en la lista si las hubiera:
		if self.matrizRehacer[-1][0]:
			self.aux_lista.append(self.matrizRehacer[-1][0])
		# Rehacer los cambios:
		self.img.imgFromPixBuf(self.matrizRehacer[-1][1][0])
		self.matrizDePixBuf.append(self.matrizRehacer[-1][1])
		self.matrizRehacer.remove(self.matrizRehacer[-1])
		self.img.updatePixBuf()
		self.imagen.set_from_pixbuf(self.img.pixBuf2)
		self.st1.push(4,"Rehacer")
		self.desBtn.set_sensitive(True)
		self.desBtnMenu.set_sensitive(True)
		if len(self.matrizRehacer)==0 :
				self.rehaBtn.set_sensitive(False)
				self.rehaBtnMenu.set_sensitive(False)

	def on_window_delete_event(self, widget, event=None):
		# Cierra la ventana principal y dentiene el bucle principal de gtk
		gtk.main_quit()	

if __name__=='__main__':
	app=ImgDistorsion()
	gtk.main()  
