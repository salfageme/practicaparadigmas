# -*- coding: utf-8 -*-
'''
PRACTICA 2 - PARADIGMAS DE PROGRAMACION: DISTORSIÓN DE IMÁGENES
AUTORES:
    DANIEL BARBA GUTIÉRREZ - 1ºGºIng.Inf - L1
    SAMUEL ALFAGEME SAINZ - 1ºGºIng.Inf - L1

'''
import os, gtk, numpy, math

class Image():

    def __init__(self, imgPath):
        self.route = imgPath
        self.img = gtk.Image()
        self.img.set_from_file(self.route)
        self.pixBuf1 = self.img.get_pixbuf()
        self.pixBuf2 = self.img.get_pixbuf()
        self.pixArray1 = self.pixBuf1.get_pixels_array()
        self.pixArray2 = self.pixArray1.copy()
        self.origDimensions = self.getDimensions()
        self.dimensions = self.getDimensions()
        self.matriz = None
        
    def getDimensions(self):
        """Devuelve una tupla con las dimensiones, ancho por alto, de la imagen"""
        return ((len(self.pixArray1[0])),(len(self.pixArray1)))
    
    def updatePixBuf(self):
        """Actualiza el buffer de trabajo pixBuf2 con los datos modificados de PixArray2"""
        self.pixBuf2 = gtk.gdk.pixbuf_new_from_array(self.pixArray2,        # @UndefinedVariable
                            self.pixBuf1.get_colorspace(), self.pixBuf1.get_bits_per_sample())  
        
    def updatePixArray(self):
        """Actualiza la informacion de la matriz base pixArray1 con los datos de pixArray2"""
        self.pixArray1 = self.pixArray2.copy()
        
    def copyBasePixArray(self):
        """Carga en la matriz de trabajo, pixArray2, los datos de la matriz base pixArray1"""
        self.pixArray2 = self.pixArray1.copy()
        
    def saveImage(self, path):
        """Guarda la imagen en la ruta introducida que se le pasa en path"""
        ext = os.path.splitext(path)[1][1:].lower()
        self.pixBuf2.save(path,ext)
        
    def imgFromPixBuf(self, pixB):
        """Reescribe la imagen empleando el PixelBuffer que recibe como parámetro"""
        self.img.set_from_pixbuf(pixB)
        self.pixBuf1 = self.img.get_pixbuf()
        self.pixArray1 = self.pixBuf1.get_pixels_array()
        self.pixArray2 = self.pixArray1.copy()
        self.dimensions = self.getDimensions()
        
    def reescalate(self, porcentaje):
        """Modifica el PixelBuffer de la imagen para reescalar su tamaño"""
        if porcentaje != 1 :
            newDim = (int(self.dimensions[0]*porcentaje), int(self.dimensions[1]*porcentaje))
        else:
            newDim = self.origDimensions
        scaledBuff = self.pixBuf2.scale_simple(newDim[0],newDim[1],gtk.gdk.INTERP_BILINEAR) # @UndefinedVariable
        self.imgFromPixBuf(scaledBuff)

    def setPreCoordMat(self,radio,fact):
        """Calculo de la matriz de precoordenadas en función del radio y el factor de la transformación"""
        self.matriz = []
        for i in range(2*radio+1):
            self.matriz.append(range(2*radio+1))
        for i in range(len(self.matriz)):
            for j in range(len(self.matriz[i])):
                x=j-radio
                y=i-radio
                tran = self.transform(x,y,radio,fact)
                self.matriz[i][j] = (x*tran, y*tran)
                
    def transform(self,x,y,r,e):
        """Devuelve, para un punto (x,y) , un radio y un factor de distorsión, el valor de la transformación del punto"""
        d = math.sqrt(x**2+y**2)
        z = d/r
        if z > 1 :
            return 1
        elif r == 0 :
            return 1
        elif z == 0:
            return 0
        elif e < 0 :
            return math.pow((1/z),(-e/(1-e)))
        else :
            return z**e
    
    def applyTransform(self, transformac):
        """Calcula y aplica las coordenadas de los pixeles que forman parte de la transformación a partir de 
        la matriz de precoordenadas."""
        matr = self.matriz
        pm = self.pixArray1
        pm2 = self.pixArray2
        xc = transformac[0]
        yc = transformac[1]
        radio = transformac[2]
        for j in range(radio*2+1):
            for i in range(radio*2+1):
                y = j - radio
                x = i - radio
                if (((xc+x>=0) & ((xc+x)<len(pm2[0]))) & ((yc+y>=0) & ((yc+y)<len(pm2)))):
                    coordX = matr[j][i][0]+xc
                    coordY = matr[j][i][1]+yc
                    if coordX >=0 and coordX<len(pm2[0]) and coordY >=0 and coordY<len(pm2):
                        pm2[yc+y][xc+x] = pm[coordY][coordX]
