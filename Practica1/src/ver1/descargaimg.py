'''
Created on 23/04/2013

@author: Samuel
'''
import urllib2, sys

URL = raw_input("Introduce la URL: ")
nombre_del_archivo = raw_input("Nombre del archivo: ")

try:
    source = urllib2.urlopen(URL).read()
except BaseException, e:
    print "Error:",e
    sys.exit()
try:
    f = open(nombre_del_archivo,"w")
    f.write(source)
except:
    print "Se produjo un problema al guardar el archivo"
    sys.exit()
f.close()