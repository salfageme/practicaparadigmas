'''
Created on 24/04/2013

@author: Samuel
'''
import DistorsionImg, urllib2, sys, os

def downloadImg():
    URL = raw_input("Introduce la URL: ")
    nombre_del_archivo = raw_input("Nombre del archivo: ")
    
    try:
        source = urllib2.urlopen(URL).read()
    except BaseException, e:
        print "Error:",e
        sys.exit()
    try:
        f = open(nombre_del_archivo,"w")
        f.write(source)
        imgDescarga = DistorsionImg.Image(f.name)
        DistorsionImg.runWImg(imgDescarga)
        #Borrar la imagen descargada: pedir confirmacion al usuario; me da pereza implementarlo ahora
        os.remove(f)
    except:
        print "Se produjo un problema al guardar el archivo"
        sys.exit()
    f.close()
downloadImg()

