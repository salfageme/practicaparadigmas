#!/usr/bin/python
# -*- coding: utf-8 -*-
import gtk,os

class descargaImg():

    def __init__(self):
        self.glade=gtk.Builder()
        self.glade.add_from_file('Descarga1.glade')
        self.ventana=self.glade.get_object('window1')
        self.ventana2=self.glade.get_object('window2')
        self.dialogoPreview=self.glade.get_object('dialog1')
        self.imgn=self.glade.get_object('image1')
        print type(self.imgn)
        # Conectar objetos con manejadores:
        self.glade.connect_signals(self)
        self.botonDescarga=self.glade.get_object('button1')
        self.botonPreview=self.glade.get_object('button2')
        self.botonOK=self.glade.get_object('button3')
        self.botonCancelar=self.glade.get_object('button3')
        self.botonCancelar=self.glade.get_object('button3')
        self.textoIntro=self.glade.get_object('url_choice')
        self.texto2=self.glade.get_object('fileNm')
        self.url_entered=""
        self.filename=""
        self.ventana.show()

    def on_button1_clicked(self,widget):
        self.url_entered = self.textoIntro.get_text()
        print self.url_entered
        self.ventana2.show()

    def on_button2_clicked(self,widget):
        self.url_entered = self.textoIntro.get_text()
        self.filename = "temp.png"
        self.descargaImagen()
        # Mostrar en el widget
        self.imgn.set_from_file("temp.png")
        
        self.dialogoPreview.show()

    def on_button3_clicked(self,widget):
        self.filename = self.texto2.get_text()
        self.descargaImagen()
        print "Pulsación en el botón"

    def on_button4_clicked(self,widget):
        self.ventana2.hide()
        print "Pulsación en el botón"

    def on_button5_clicked(self,widget):
        # Destruir la imagen (se cancela la descarga de la imagen)
        os.remove(self.filename)
        self.dialogoPreview.hide()

    def on_button6_clicked(self,widget):
        # OK: botón de confirmacion
        print "Botón de confirmación"
        self.dialogoPreview.hide()

    def on_window1_delete_event(self,widget,event):
        print "Señal para cerrar la ventana"
        gtk.main_quit()

    def on_window2_delete_event(self,window,event):
        window.hide()
        return True                                         # devolver True como parámetro para que la ventana se oculte y no se destruya

    def on_dialog1_delete_event(self,window,event):
        window.hide()
        return True

    def descargaImagen(self):
        import urllib2, sys

        URL = self.url_entered
        nombre_del_archivo = self.filename
        try:
            source = urllib2.urlopen(URL).read()
        except BaseException, e:
            print "Error:",e
            sys.exit()
        try:
            f = open(nombre_del_archivo,"wb")
            f.write(source)
        except:
            print "Se produjo un problema al guardar el archivo"
            sys.exit()
        f.close()

class Image():
    # Define un objeto imagen, con los atributos concretos que emplearemos para la practica y metodos para su manipulacion
    def __init__(self, imgPath):
        self.isValid = False
        while not(self.isValid):
            try:
                self.route = imgPath
                self.img = gtk.Image()
                self.img.set_from_file(self.route)
                self.pixBuf1 = self.img.get_pixbuf()
                self.pixBuf2 = None
                self.pixArray1 = self.pixBuf1.get_pixels_array()
                self.pixArray2 = self.pixArray1.copy()
                self.dimensions = self.getDimensions()
                self.isValid = True
            except:
                self.route = raw_input("No es posible cargar la imagen, introduzca una nueva: ")
        
# Para lanzar la aplicación...
if __name__=='__main__':
    # Instanciamos la interfaz
    app=descargaImg()
    gtk.main()
