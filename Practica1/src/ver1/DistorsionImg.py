# -*- coding: utf-8 -*-
'''
PRACTICA 1 - PARADIGMAS DE PROGRAMACION: DISTORSIÓN DE IMÁGENES
AUTORES:
    DANIEL BARBA GUTIÉRREZ - 1ºGºIng.Inf - L1
    SAMUEL ALFAGEME SAINZ - 1ºGºIng.Inf - L1

'''
import os, gtk, numpy, math

def run():
        filename = raw_input("Nombre del fichero origen: ")
        while not os.path.isfile(filename):
            filename = raw_input("El fichero introducido no existe, por favor, especifique un archivo valido: ")
        print("Leyendo el archivo: " + filename )
        imagen = Image(filename)
        print ("Las dimensiones de la imagen cargada son: " + imagen.getDimensions())
        print "Introduzca puntos de distorsión: xc yc r e: "
        transformaciones = listaTransformacion()
        aplyTransform(imagen,transformaciones)
        imagen.saveImage()

def runWImg(imagen):
        print ("Las dimensiones de la imagen cargada son: " + imagen.getDimensions())
        print "Introduzca puntos de distorsión: xc yc r e: "
        transformaciones = listaTransformacion()
        aplyTransform(imagen,transformaciones)
        imagen.saveImage()
        

class Image():
    # Define un objeto imagen, con los atributos concretos que emplearemos para la practica y metodos para su manipulacion
    def __init__(self, imgPath):
        self.isValid = False
        while not(self.isValid):
            try:
                self.route = imgPath
                self.img = gtk.Image()
                self.img.set_from_file(self.route)
                self.pixBuf1 = self.img.get_pixbuf()
                self.pixBuf2 = None
                self.pixArray1 = self.pixBuf1.get_pixels_array()
                self.pixArray2 = self.pixArray1.copy()
                self.dimensions = self.getDimensions()
                self.isValid = True
            except:
                self.route = raw_input("No es posible cargar la imagen, introduzca una nueva: ")
    def getDimensions(self):
        # Devuelve un string con las dimensiones de la imagen cargada en formato "ANCHO x ALTO"
        return str(len(self.pixArray1[0]))+"x"+str(len(self.pixArray1))
    def saveImage(self):
        # Metodo para el guardado en disco de la imagen
        imgIsSaved = False
        while not(imgIsSaved):
            imgRoute = raw_input("Nombre del fichero destino: ")
            try:
                self.pixBuf2 = gtk.gdk.pixbuf_new_from_array(self.pixArray2, self.pixBuf1.get_colorspace(), self.pixBuf1.get_bits_per_sample())  # @UndefinedVariable
                ext = os.path.splitext(imgRoute)[1][1:].lower()
                self.pixBuf2.save(imgRoute,ext)
                imgIsSaved = True
                print "Se ha guardado la imagen en ", imgRoute 
            except:
                print("No se ha podido guardar la imagen.")
        
def listaTransformacion():
    # Procedimiento para la generacion de una lista de tuplas de 4 elementos: coordenada central X e Y , radio y 
    # grado de transformacion correspondientes a las distintas transformaciones que se efectuaran sobre la imagen.
    listTrans = []
    userChoice = None
    while userChoice != () or userChoice == None:
        usList = []
        userChoice = raw_input("xc yc r e: ")
        if userChoice != "":
            userChoice = userChoice.split()
            if len(userChoice) == 4 :
                try:
                    for n in range(3):
                        usList.append(int(userChoice[n]))
                    usList.append(float(userChoice[3]))
                    userChoice = tuple(usList)
                    listTrans.append(userChoice)
                except:
                    print "El formato de los parametros introducidos no es correcto"
            else:
                print "Introducidos mas parametros de los esperados, la transformacion no tendra efecto"
        else:    
            return listTrans

def transform(x,y,r,e):
    # Metodo para el calculo de la funcion de transformacion.
    d = math.sqrt(x**2+y**2)
    z = d/r
    if z > 1 :
        return 1
    elif r == 0 :
        return 1
    elif z == 0:
        return 0
    elif e < 0 :
        return math.pow((1/z),(-e/(1-e)))
    else :
        return z**e

def preCoordMat(radio,fact):
    # Funcion generadora de una matriz cuadrada de coordenadas relativas (tuplas (x,y)) para un radio dado a la aplicacion
    # de una transformacion de factor 'fact'.
    matri = []
    for i in range(2*radio+1):
        matri.append(range(2*radio+1))
    for i in range(len(matri)):
        for j in range(len(matri[i])):
            x=j-radio
            y=i-radio
            tran = transform(x,y,radio,fact)
            matri[i][j] = (x*tran, y*tran)
    return matri

def aplyTransform(img, transformac):
    # Procedimiento de aplicacion de las transformaciones sobre un objeto imagen, dada una lista de las mismas.
    # Aplica cada transformacion de la lista: 'trans' de forma separada, y actualiza el pixelbuffer de la imagen una vez
    # aplicadas.
    pm = img.pixArray1
    pm2 = img.pixArray2
    print "Aplicando las transformaciones..."
    for trans in transformac:
        xc = trans[0]
        yc = trans[1]
        radio = trans[2]
        factor = trans[3]
        matr = preCoordMat(radio,factor)
        for j in range(radio*2+1):
            for i in range(radio*2+1):
                y = j - radio
                x = i - radio
                if (((xc+x>=0) & ((xc+x)<len(pm2[0]))) & ((yc+y>=0) & ((yc+y)<len(pm2)))):
                    coordX = matr[j][i][0]+xc
                    coordY = matr[j][i][1]+yc
                    if coordX >=0 and coordX<len(pm2[0]) and coordY >=0 and coordY<len(pm2):
                        pm2[yc+y][xc+x] = pm[coordY][coordX]
        pm = pm2.copy()

run()