PYSTORSION
=========
---
PYSTORSION es un programa de distorsión de imágenes desarrollado como práctica de la asignatura [Paradigmas de Programación][paradigmas] de la Universidad de Valladolid por [Samuel Alfageme][1] y [Daniel Barba][2]

 
Versión
-
1.4

Dependencias
--------------
* Python 2.7
* Gtk 2.24
* Numpy 1.7
* PyGtk 2.24

Funcionamiento
-------------
Descomprimir el fichero pystorsion.tar y ejecutarlo:
```
tar -xvf pystorsion.tar
python src/pystorsion.py
```
Abrir un fichero imagen pulsando `ctrl+O` y modificar los parámetros de distorsión con los controles superiores. 
Pulsar sobre la imagen y arrastrar el ratón para previsualizar el resultado de la distorsión.

Licencia
-

GNU

  [1]: http://twitter.com/samu_alfageme
  [2]: http://twitter.com/corik87
  [paradigmas]: http://www.infor.uva.es/~cvaca/asigs/paradigmas.html
  
